/*
Author: John Hultman
Class: CSCI 441
Date: 2/16/2020
Description: lab 4 Transformation Matrices
*/
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>

/*
	openGL matrix format?
	_____		 _____
	|	0	4	8  12|
	|	1	5	9  13|
	|	2	6  10  14|
	|  	3  7  11  15|
	|____		 ____|

    0   1   2   3
    4   5   6   7
    8   9   10  11
    12  13  14  15
*/

int p =0;
class Matrix
{
public:
    float matrix[16] = {
            1.0,  0.0,  0.0,  0.0,
            0.0,  1.0,  0.0,  0.0,
            0.0,  0.0,  1.0,  0.0,
            0.0,  0.0,  0.0,  1.0
    };
    Matrix()//identity matrix for no transform;
    {
        matrix[0] = 1.0;
        matrix[1] = 0.0;
        matrix[2] = 0.0;
        matrix[3] = 0.0;
        matrix[4] = 0.0;
        matrix[5] = 1.0;
        matrix[6] = 0.0;
        matrix[7] = 0.0;
        matrix[8] = 0.0;
        matrix[9] = 0.0;
        matrix[10] = 1.0;
        matrix[11] = 0.0;
        matrix[12] = 0.0;
        matrix[13] = 0.0;
        matrix[14] = 0.0;
        matrix[15] = 1.0;
    }
    Matrix static scale(Matrix inMatrix, float scaleX, float scaleY, float scaleZ)//scale transform matrix
    {
        inMatrix.matrix[0]= scaleX;
        inMatrix.matrix[1] = 0.0;
        inMatrix.matrix[2] = 0.0;
        inMatrix.matrix[3] = 0.0;
        inMatrix.matrix[4] = 0.0;
        inMatrix.matrix[5] = scaleY;
        inMatrix.matrix[6] = 0.0;
        inMatrix.matrix[7] = 0.0;
        inMatrix.matrix[8] = 0.0;
        inMatrix.matrix[9] = 0.0;
        inMatrix.matrix[10] = scaleZ;
        inMatrix.matrix[11] = 0.0;
        inMatrix.matrix[12] = 0.0;
        inMatrix.matrix[13] = 0.0;
        inMatrix.matrix[14] = 0.0;
        inMatrix.matrix[15] = 1.0;

        return inMatrix;
    }
    Matrix static translate(Matrix inMatrix, float transX, float transY, float transZ)//translate transform matrix
    {
        inMatrix.matrix[0] = 1.0;
        inMatrix.matrix[1] = 0.0;
        inMatrix.matrix[2] = 0.0;
        inMatrix.matrix[3] = transX;
        inMatrix.matrix[4] = 0.0;
        inMatrix.matrix[5] = 1.0;
        inMatrix.matrix[6] = 0.0;
        inMatrix.matrix[7] = transY;
        inMatrix.matrix[8] = 0.0;
        inMatrix.matrix[9] = 0.0;
        inMatrix.matrix[10] = 1.0;
        inMatrix.matrix[11] = transZ;
        inMatrix.matrix[12] = 0.0;
        inMatrix.matrix[13] = 0.0;
        inMatrix.matrix[14] = 0.0;
        inMatrix.matrix[15] = 1.0;

        return inMatrix;
    }
    Matrix static rotateX(Matrix inMatrix, float theta)
    {
        inMatrix.matrix[0] = 1.0;
        inMatrix.matrix[1] = 0.0;
        inMatrix.matrix[2] = 0.0;
        inMatrix.matrix[3] = 0.0;
        inMatrix.matrix[4] = 0.0;
        inMatrix.matrix[5] = cos(theta);
        inMatrix.matrix[6] = -sin(theta);
        inMatrix.matrix[7] = 0.0;
        inMatrix.matrix[8] = 0.0;
        inMatrix.matrix[9] = sin(theta);
        inMatrix.matrix[10] = cos(theta);
        inMatrix.matrix[11] = 0.0;
        inMatrix.matrix[12] = 0.0;
        inMatrix.matrix[13] = 0.0;
        inMatrix.matrix[14] = 0.0;
        inMatrix.matrix[15] = 1.0;

        return inMatrix;
    }
    Matrix static rotateY(Matrix inMatrix, float theta)
    {
        inMatrix.matrix[0] = cos(theta);
        inMatrix.matrix[1] = 0.0;
        inMatrix.matrix[2] = sin(theta);
        inMatrix.matrix[3] = 0.0;
        inMatrix.matrix[4] = 0.0;
        inMatrix.matrix[5] = 1.0;
        inMatrix.matrix[6] = 0.0;
        inMatrix.matrix[7] = 0.0;
        inMatrix.matrix[8] = -(sin(theta));
        inMatrix.matrix[9] = 0.0;
        inMatrix.matrix[10] = cos(theta);
        inMatrix.matrix[11] = 0.0;
        inMatrix.matrix[12] = 0.0;
        inMatrix.matrix[13] = 0.0;
        inMatrix.matrix[14] = 0.0;
        inMatrix.matrix[15] = 1.0;

        return inMatrix;
    }
    Matrix static rotateZ(Matrix inMatrix, float theta)
    {
        inMatrix.matrix[0] = cos(theta);
        inMatrix.matrix[1] = -(sin(theta));
        inMatrix.matrix[2] = 0.0;
        inMatrix.matrix[3] = 0.0;
        inMatrix.matrix[4] = sin(theta);
        inMatrix.matrix[5] = cos(theta);
        inMatrix.matrix[6] = 0.0;
        inMatrix.matrix[7] = 0.0;
        inMatrix.matrix[8] = 0.0;
        inMatrix.matrix[9] = 0.0;
        inMatrix.matrix[10] = 1.0;
        inMatrix.matrix[11] = 0.0;
        inMatrix.matrix[12] = 0.0;
        inMatrix.matrix[13] = 0.0;
        inMatrix.matrix[14] = 0.0;
        inMatrix.matrix[15] = 1.0;

        return inMatrix;
    }
    Matrix static perspec(Matrix inMatrix, float l, float r, float t, float b, float n, float f)
    {
        inMatrix.matrix[0] = 2 * n / (r - l);
        inMatrix.matrix[1] = 0.0;
        inMatrix.matrix[2] = (r + l) / (r - l);
        inMatrix.matrix[3] = 0.0;
        inMatrix.matrix[4] = 0.0;
        inMatrix.matrix[5] = 2*n / (t - b);
        inMatrix.matrix[6] = (t + b) / (t - b);
        inMatrix.matrix[7] = 0.0;
        inMatrix.matrix[8] = 0.0;
        inMatrix.matrix[9] = 0.0;
        inMatrix.matrix[10] = -(f + n) / (f - n);
        inMatrix.matrix[11] = -(2 * f * n) / (f - n);
        inMatrix.matrix[12] = 0.0;
        inMatrix.matrix[13] = 0.0;
        inMatrix.matrix[14] = -1.0;
        inMatrix.matrix[15] = 0.0;

        return inMatrix;
    }
    Matrix static ortho(Matrix inMatrix, float l, float r, float t, float b, float n, float f)
    {
        inMatrix.matrix[0] = 2 / (r - l);
        inMatrix.matrix[1] = 0.0;
        inMatrix.matrix[2] = 0.0;
        inMatrix.matrix[3] = -(r + l) / (r - l);
        inMatrix.matrix[4] = 0.0;
        inMatrix.matrix[5] = 2 / (t - b);
        inMatrix.matrix[6] = 0.0;
        inMatrix.matrix[7] = -(t + b) / (t - b);
        inMatrix.matrix[8] = 0.0;
        inMatrix.matrix[9] = 0.0;
        inMatrix.matrix[10] = -2 / (f - n);
        inMatrix.matrix[11] = -(f + n) / (f - n);
        inMatrix.matrix[12] = 0.0;
        inMatrix.matrix[13] = 0.0;
        inMatrix.matrix[14] = 0.0;
        inMatrix.matrix[15] = 1.0;

        return inMatrix;
    }
    Matrix static cameraLook(Matrix inMatrix, float Rx, float Ry, float Rz, float Ux, float Uy, float Uz, float Dx, float Dy, float Dz)
    {
        inMatrix.matrix[0] = Rx;
        inMatrix.matrix[1] = Ry;
        inMatrix.matrix[2] = Rz;
        inMatrix.matrix[3] = 0.0;
        inMatrix.matrix[4] = Ux;
        inMatrix.matrix[5] = Uy;
        inMatrix.matrix[6] = Uz;
        inMatrix.matrix[7] = 0.0;
        inMatrix.matrix[8] = Dx;
        inMatrix.matrix[9] = Dy;
        inMatrix.matrix[10] = Dz;
        inMatrix.matrix[11] = 0.0;
        inMatrix.matrix[12] = 0.0;
        inMatrix.matrix[13] = 0.0;
        inMatrix.matrix[14] = 0.0;
        inMatrix.matrix[15] = 1.0;

        return inMatrix;
    }
    Matrix static cameraPos(Matrix inMatrix, float cameraX, float cameraY, float cameraZ)
    {
        inMatrix.matrix[0] = 1.0;
        inMatrix.matrix[1] = 0.0;
        inMatrix.matrix[2] = 0.0;
        inMatrix.matrix[3] = -cameraX;
        inMatrix.matrix[4] = 0.0;
        inMatrix.matrix[5] = 1.0;
        inMatrix.matrix[6] = 0.0;
        inMatrix.matrix[7] = -cameraY;
        inMatrix.matrix[8] = 0.0;
        inMatrix.matrix[9] = 0.0;
        inMatrix.matrix[10] = 1.0;
        inMatrix.matrix[11] = -cameraZ;
        inMatrix.matrix[12] = 0.0;
        inMatrix.matrix[13] = 0.0;
        inMatrix.matrix[14] = 0.0;
        inMatrix.matrix[15] = 1.0;

        return inMatrix;
    }
    Matrix static viewPort(Matrix inMatrix, float Nx, float Ny)
    {
        inMatrix.matrix[0] = Nx / 2;
        inMatrix.matrix[1] = 0.0;
        inMatrix.matrix[2] = 0.0;
        inMatrix.matrix[3] = (Nx - 1) / 2;
        inMatrix.matrix[4] = 0.0;
        inMatrix.matrix[5] = Ny / 2;
        inMatrix.matrix[6] = 0.0;
        inMatrix.matrix[7] = (Ny - 1) / 2;
        inMatrix.matrix[8] = 0.0;
        inMatrix.matrix[9] = 0.0;
        inMatrix.matrix[10] = 1.0;
        inMatrix.matrix[11] = 0.0;
        inMatrix.matrix[12] = 0.0;
        inMatrix.matrix[13] = 0.0;
        inMatrix.matrix[14] = 0.0;
        inMatrix.matrix[15] = 1.0;

        return inMatrix;
    }
};

Matrix operator * (Matrix inM1, Matrix inM2)
{
    Matrix outM;

    outM.matrix[0] = inM1.matrix[0]*inM2.matrix[0]+inM1.matrix[4]*inM2.matrix[1]+inM1.matrix[8]*inM2.matrix[2]+inM1.matrix[12]*inM2.matrix[3];
    outM.matrix[1] = inM1.matrix[0]*inM2.matrix[4]+inM1.matrix[4]*inM2.matrix[5]+inM1.matrix[8]*inM2.matrix[6]+inM1.matrix[12]*inM2.matrix[7];
    outM.matrix[2] = inM1.matrix[0]*inM2.matrix[8]+inM1.matrix[4]*inM2.matrix[9]+inM1.matrix[8]*inM2.matrix[10]+inM1.matrix[12]*inM2.matrix[11];
    outM.matrix[3] = inM1.matrix[0]*inM2.matrix[12]+inM1.matrix[4]*inM2.matrix[13]+inM1.matrix[8]*inM2.matrix[14]+inM1.matrix[12]*inM2.matrix[15];

    outM.matrix[4] = inM1.matrix[1]*inM2.matrix[0]+inM1.matrix[5]*inM2.matrix[1]+inM1.matrix[9]*inM2.matrix[2]+inM1.matrix[13]*inM2.matrix[3];
    outM.matrix[5] = inM1.matrix[1]*inM2.matrix[4]+inM1.matrix[5]*inM2.matrix[5]+inM1.matrix[9]*inM2.matrix[6]+inM1.matrix[13]*inM2.matrix[7];
    outM.matrix[6] = inM1.matrix[1]*inM2.matrix[8]+inM1.matrix[5]*inM2.matrix[9]+inM1.matrix[9]*inM2.matrix[10]+inM1.matrix[13]*inM2.matrix[11];
    outM.matrix[7] = inM1.matrix[1]*inM2.matrix[12]+inM1.matrix[5]*inM2.matrix[13]+inM1.matrix[9]*inM2.matrix[14]+inM1.matrix[13]*inM2.matrix[15];

    outM.matrix[8] = inM1.matrix[2]*inM2.matrix[0]+inM1.matrix[6]*inM2.matrix[1]+inM1.matrix[10]*inM2.matrix[2]+inM1.matrix[14]*inM2.matrix[3];
    outM.matrix[9] = inM1.matrix[2]*inM2.matrix[4]+inM1.matrix[6]*inM2.matrix[5]+inM1.matrix[10]*inM2.matrix[6]+inM1.matrix[14]*inM2.matrix[7];
    outM.matrix[10] = inM1.matrix[2]*inM2.matrix[8]+inM1.matrix[6]*inM2.matrix[9]+inM1.matrix[10]*inM2.matrix[10]+inM1.matrix[14]*inM2.matrix[11];
    outM.matrix[11] = inM1.matrix[2]*inM2.matrix[12]+inM1.matrix[6]*inM2.matrix[13]+inM1.matrix[10]*inM2.matrix[14]+inM1.matrix[14]*inM2.matrix[15];

    outM.matrix[12] = inM1.matrix[3]*inM2.matrix[0]+inM1.matrix[7]*inM2.matrix[1]+inM1.matrix[11]*inM2.matrix[2]+inM1.matrix[15]*inM2.matrix[3];
    outM.matrix[13] = inM1.matrix[3]*inM2.matrix[4]+inM1.matrix[7]*inM2.matrix[5]+inM1.matrix[11]*inM2.matrix[6]+inM1.matrix[15]*inM2.matrix[7];
    outM.matrix[14] = inM1.matrix[3]*inM2.matrix[8]+inM1.matrix[7]*inM2.matrix[9]+inM1.matrix[11]*inM2.matrix[10]+inM1.matrix[15]*inM2.matrix[11];
    outM.matrix[15] = inM1.matrix[3]*inM2.matrix[12]+inM1.matrix[7]*inM2.matrix[13]+inM1.matrix[11]*inM2.matrix[14]+inM1.matrix[15]*inM2.matrix[15];
    
    return outM;
}

std::ostream& operator << (std::ostream& out, Matrix inM)
{
    int k = 0;
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            out << inM.matrix[k] << "   ";
            k++;
        }
       out << std::endl;
    }
    return out;
}

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window, Shader &shader) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
    {
        switch (p) {
            case 0: //option 1 is to make the image static
                std::cout << "switching to perspective" << std::endl;
                p = 1;
                break;
            case 1: //option 2 is scale
                std::cout << "switching to orthographic" << std::endl;
                p = 0;
                break;
        }
    }
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the model */
    float vertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
    };

    // copy vertex data
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);
    //camera matrix initilize
    int cameraLoc = glGetUniformLocation(shader.id(), "camera");
    Matrix cameraMatrix = Matrix();
    Matrix cameraPos = Matrix();
    cameraPos = Matrix::cameraPos(cameraPos, 0.0, 0.0, -5.0);
    Matrix cameraLook = Matrix();
    cameraLook = Matrix::cameraLook(cameraLook, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0);
    cameraMatrix = cameraLook * cameraPos;
    glUniformMatrix4fv(cameraLoc, 1, GL_FALSE, cameraMatrix.matrix);

    int viewPortLoc = glad_glGetUniformLocation(shader.id(), "viewPort");
    Matrix viewPort = Matrix();
    viewPort = Matrix::viewPort(viewPort, 1.0, 1.0);
    glad_glUniformMatrix4fv(viewPortLoc, 1, GL_FALSE, viewPort.matrix);



    int modelLoc = glGetUniformLocation(shader.id(), "model");
    Matrix scaleMatrix = Matrix();
    Matrix translateMatrix = Matrix();
    Matrix modelMatrix = Matrix();
    Matrix rotateMatrix = Matrix();

    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, modelMatrix.matrix);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {


        // process input
        processInput(window, shader);
        glfwSetKeyCallback(window, key_callback);

        float time = glfwGetTime();

        rotateMatrix = (Matrix::rotateZ(rotateMatrix, time)) * (Matrix::rotateY(rotateMatrix, time)) * (Matrix::rotateX(rotateMatrix, time));
        scaleMatrix = Matrix::scale(scaleMatrix, sin(time), sin(time), sin(time));
        translateMatrix = Matrix::translate(translateMatrix, sin(time), sin(time), sin(time));
        modelMatrix = rotateMatrix * scaleMatrix * translateMatrix;
        modelLoc = glGetUniformLocation(shader.id(), "model");
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, modelMatrix.matrix);

        int projLoc = glad_glGetUniformLocation(shader.id(), "projection");
        Matrix projMatrix = Matrix();
        if (p == 0)
        {
            projMatrix = Matrix::ortho(projMatrix, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0);
        }
         if (p == 1)
        {
            projMatrix = Matrix::perspec(projMatrix,-1.0, 1.0, 1.0, -1.0, -1.0, 1.0);
        }
        glad_glUniformMatrix4fv(projLoc, 1, GL_FALSE, projMatrix.matrix);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // activate shader
        shader.use();

        // render the cube
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(vertices));

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
