#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

uniform mat4 model;//3d object transform
uniform mat4 camera;//camera transform
uniform mat4 projection;//perspective or orthographic projection transform
uniform mat4 viewPort;//viewport transform

out vec3 ourColor;

void main() {
    gl_Position = viewPort * projection * camera * model * vec4(aPos, 1.0);
    ourColor = aColor;
}
