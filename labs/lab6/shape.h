#ifndef _CSCI441_SHAPE_H_
#define _CSCI441_SHAPE_H_

#include <cstdlib>
#include <vector>

template <typename T, typename N, typename C>
void add_vertex(T& coords, const N& x, const N& y, const N& z,
        const C& r, const C& g, const C& b,
        const Vector4& n=Vector4(1,0,0), bool with_noise=false) {
    // adding color noise makes it easier to see before shading is implemented
    float noise = 1-with_noise*(rand()%150)/100.;
    coords.push_back(x);
    coords.push_back(y);
    coords.push_back(z);

    coords.push_back(r*noise);
    coords.push_back(g*noise);
    coords.push_back(b*noise);

    Vector4 normal = n.normalized();
    coords.push_back(normal.x());
    coords.push_back(normal.y());
    coords.push_back(normal.z());
}

template <typename T, typename N, typename C>
void add_vertex(T& coords, T& vectors, T& colors, T& normals, const N& x, const N& y, const N& z,
                const C& r, const C& g, const C& b,
                const Vector4& n=Vector4(1,0,0), bool with_noise=false) {
    // adding color noise makes it easier to see before shading is implemented
    float noise = 1-with_noise*(rand()%150)/100.;
    vectors.push_back(x);
    vectors.push_back(y);
    vectors.push_back(z);

    vectors.push_back(r);
    vectors.push_back(g);
    vectors.push_back(b);

    Vector4 normal = n.normalized();
    normals.push_back(normal.x());
    normals.push_back(normal.y());
    normals.push_back(normal.z());

    coords.push_back(x);
    coords.push_back(y);
    coords.push_back(z);

    coords.push_back(r*noise);
    coords.push_back(g*noise);
    coords.push_back(b*noise);

    normal = n.normalized();
    coords.push_back(normal.x());
    coords.push_back(normal.y());
    coords.push_back(normal.z());
}

Vector4 find_flat_normals(Vector4 a, Vector4 b, Vector4 c)
{
    Vector4 cross = (b - a).cross((c - a));
    cross.normalized();
    Vector4 normal(cross.x(), cross.y(), cross.z());
    return normal;
}

void find_smooth_normals(std::vector<Vector4> inPos, std::vector<Vector4> inNormals)
{
    std::vector<Vector4> outNormals;
    Vector4 totalNorm = Vector4(0, 0, 0);
    Vector4 outNorm;
    int count = 0;
    for (int i=0; i < inPos.size(); i++)
    {
        Vector4 currentPos = inPos[i];
        Vector4 currentNorm = inNormals[i];
        for (int j = 0; j < inPos.size(); j++)
        {
            Vector4 nextPos = inPos[j];
            Vector4 nextNorm = inNormals[j];
            if (currentPos == nextPos)
            {
                totalNorm = totalNorm + currentNorm + nextNorm;
                count++;
            }
        }

        outNorm = totalNorm / 2;
        outNorm.normalized();

        outNormals.push_back(outNorm);
    }
    inNormals = outNormals;
}

void redo_coords(std::vector<float> inCoords, std::vector<Vector4> inPos, std::vector<Vector4> inColors, std::vector<Vector4> inNormals)
{
    std::vector<float> tempCoords;
    for (int i=0; i < inPos.size(); i++)
    {
        tempCoords.push_back(inPos[i].x());
        tempCoords.push_back(inPos[i].y());
        tempCoords.push_back(inPos[i].z());

        tempCoords.push_back(inColors[i].x());
        tempCoords.push_back(inColors[i].y());
        tempCoords.push_back(inColors[i].z());

        tempCoords.push_back(inNormals[i].x());
        tempCoords.push_back(inNormals[i].y());
        tempCoords.push_back(inNormals[i].z());
    }
    inCoords = tempCoords;
}

class DiscoCube {
public:
    std::vector<float> coords;
    DiscoCube() : coords{
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,

        -0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,
        -0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,

        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  1.0f,  0.0,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  1.0f,  0.0,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0
    } {}

};

class Cylinder {
public:
    std::vector<float> coords;
    Cylinder(unsigned int n, float r, float g, float b) {
        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            // vertex i
            double theta_i = i*step_size;
            double vi_x = radius*cos(theta_i);
            double vi_y = radius*sin(theta_i);

            // vertex i+1
            double theta_ip1 = ((i+1)%n)*step_size;
            double vip1_x = radius*cos(theta_ip1);
            double vip1_y = radius*sin(theta_ip1);

            add_vertex(coords, vi_x, -h, vi_y, r, g, b);
            add_vertex(coords, vi_x, h, vi_y, r, g, b);
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);

            // add triangle vip1L, viH, vip1H
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);
            add_vertex(coords, vi_x, h, vi_y, r, g, b);
            add_vertex(coords, vip1_x, h, vip1_y, r, g, b);

            // add high triangle vi, vip1, 0
            Vector4 nh(0, 1, 0);
            add_vertex(coords, vip1_x, h, vip1_y, r, g, b);
            add_vertex(coords, vi_x, h, vi_y, r, g, b);
            add_vertex(coords, c_x, h, c_y, r, g, b);

            // // add low triangle vi, vip1, 0
            Vector4 nl(0, -1, 0);
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);
            add_vertex(coords, c_x, -h, c_y, r, g, b);
            add_vertex(coords, vi_x, -h, vi_y, r, g, b);
        }
    }
};

class Cone {
public:
    std::vector<float> coords;
    Cone(unsigned int n, float r, float g, float b) {

        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            // vertex i
            double theta_i = i*step_size;
            double vi_x = radius*cos(theta_i);
            double vi_y = radius*sin(theta_i);

            // vertex i+1
            double theta_ip1 = ((i+1)%n)*step_size;
            double vip1_x = radius*cos(theta_ip1);
            double vip1_y = radius*sin(theta_ip1);

            // add triangle viL, viH, vip1L
            add_vertex(coords, vi_x, -h, vi_y, r, g, b);
            add_vertex(coords, c_x, h, c_y, r, g, b);
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);

            // // add low triangle vi, vip1, 0
            add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);
            add_vertex(coords, c_x, -h, c_y, r, g, b);
            add_vertex(coords, vi_x, -h, vi_y, r, g, b);
        }
    }
};

class Sphere {
    double x(float r, float phi, float theta){
        return r*cos(theta)*sin(phi);
    }

    double y(float r, float phi, float theta){
        return r*sin(theta)*sin(phi);
    }

    double z(float r, float phi, float theta){
        return r*cos(phi);
    }

public:
    std::vector<float> coords;
    Sphere(unsigned int n, float radius, float r, float g, float b) {
        int n_steps = (n%2==0) ? n : n+1;
        double step_size = 2*M_PI / n_steps;

        for (int i = 0; i < n_steps/2.0; ++i) {
            for (int j = 0; j < n_steps; ++j) {
                double phi_i = i*step_size;
                double phi_ip1 = ((i+1)%n_steps)*step_size;
                double theta_j = j*step_size;
                double theta_jp1 = ((j+1)%n_steps)*step_size;

                // vertex i,j
                double vij_x = x(radius, phi_i, theta_j);
                double vij_y = y(radius, phi_i, theta_j);
                double vij_z = z(radius, phi_i, theta_j);

                // vertex i+1,j
                double vip1j_x = x(radius, phi_ip1, theta_j);
                double vip1j_y = y(radius, phi_ip1, theta_j);
                double vip1j_z = z(radius, phi_ip1, theta_j);

                // vertex i,j+1
                double vijp1_x = x(radius, phi_i, theta_jp1);
                double vijp1_y = y(radius, phi_i, theta_jp1);
                double vijp1_z = z(radius, phi_i, theta_jp1);

                // vertex i+1,j+1
                double vip1jp1_x = x(radius, phi_ip1, theta_jp1);
                double vip1jp1_y = y(radius, phi_ip1, theta_jp1);
                double vip1jp1_z = z(radius, phi_ip1, theta_jp1);

                // add triangle
                Vector4 norm = find_flat_normals(Vector4(vij_x, vij_y, vij_z), Vector4(vip1j_x, vip1j_y, vip1j_z), Vector4(vijp1_x, vijp1_y, vijp1_z));
                add_vertex(coords, vij_x, vij_y, vij_z, r, g, b, norm);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b, norm);
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b, norm);

                // add triangle
                Vector4 norm2 = find_flat_normals(Vector4(vijp1_x, vijp1_y, vijp1_z), Vector4(vip1jp1_x, vip1jp1_y, vip1jp1_z), Vector4(vip1j_x, vip1j_y, vip1j_z));
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b, norm2);
                add_vertex(coords, vip1jp1_x, vip1jp1_y, vip1jp1_z, r, g, b, norm2);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b, norm2);
            }
        }
    }
};

class Torus {
    double x(float c, float a, float phi, float theta) {
        return (c+a*cos(theta))*cos(phi);
    }

    double y(float c, float a, float phi, float theta) {
        return (c+a*cos(theta))*sin(phi);
    }

    double z(float c, float a, float phi, float theta) {
        return a*sin(theta);
    }

public:
    std::vector<float> coords;
    std::vector<Vector4> pos;
    std::vector<Vector4> colors;
    std::vector<Vector4> norms;
    Torus(unsigned int n, float c, float a, float r, float g, float b) {

        double step_size = 2*M_PI / n;
        double c_x=0;
        double c_y=0;
        double h = .5;
        float radius = .5;

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                double phi_i = i*step_size;
                double phi_ip1 = ((i+1)%n)*step_size;
                double theta_j = j*step_size;
                double theta_jp1 = ((j+1)%n)*step_size;

                // vertex i,j
                double vij_x = x(c, a, phi_i, theta_j);
                double vij_y = y(c, a, phi_i, theta_j);
                double vij_z = z(c, a, phi_i, theta_j);

                // vertex i+1,j
                double vip1j_x = x(c, a, phi_ip1, theta_j);
                double vip1j_y = y(c, a, phi_ip1, theta_j);
                double vip1j_z = z(c, a, phi_ip1, theta_j);

                // vertex i,j+1
                double vijp1_x = x(c, a, phi_i, theta_jp1);
                double vijp1_y = y(c, a, phi_i, theta_jp1);
                double vijp1_z = z(c, a, phi_i, theta_jp1);

                // vertex i+1,j+1
                double vip1jp1_x = x(c, a, phi_ip1, theta_jp1);
                double vip1jp1_y = y(c, a, phi_ip1, theta_jp1);
                double vip1jp1_z = z(c, a, phi_ip1, theta_jp1);

                //find normals for flat shading
                Vector4 norm = find_flat_normals( Vector4(vij_x, vij_y, vij_z), Vector4(vip1j_x, vip1j_y, vip1j_z), Vector4(vijp1_x, vijp1_y, vijp1_z));
                Vector4 norm2 = find_flat_normals(Vector4(vip1jp1_x, vip1jp1_y, vip1jp1_z), Vector4(vijp1_x, vijp1_y, vijp1_z), Vector4(vip1j_x, vip1j_y, vip1j_z));


                add_vertex(coords, vij_x, vij_y, vij_z, r, g, b, norm);
                pos.push_back(Vector4(vij_x, vij_y, vij_z));
                colors.push_back(Vector4(r, g, b));
                norms.push_back(norm);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b, norm);
                pos.push_back(Vector4(vip1j_x, vip1j_y, vip1j_z));
                colors.push_back(Vector4(r, g, b));
                norms.push_back(norm);
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b, norm);
                pos.push_back(Vector4(vijp1_x, vijp1_y, vijp1_z));
                colors.push_back(Vector4(r, g, b));
                norms.push_back(norm);


                // add triangle
                add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b, norm2);
                pos.push_back(Vector4(vijp1_x, vijp1_y, vijp1_z));
                colors.push_back(Vector4(r, g, b));
                norms.push_back(norm2);
                add_vertex(coords, vip1jp1_x, vip1jp1_y, vip1jp1_z, r, g, b, norm2);
                pos.push_back(Vector4(vip1jp1_x, vip1jp1_y, vip1jp1_z));
                colors.push_back(Vector4(r, g, b));
                norms.push_back(norm2);
                add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b, norm2);
                pos.push_back(Vector4(vip1j_x, vip1j_y, vip1j_z));
                colors.push_back(Vector4(r, g, b));
                norms.push_back(norm2);

                /*
                // add triangle
                add_vertex(coords, pos, color, norms, vij_x, vij_y, vij_z, r, g, b, norm);
                add_vertex(coords, pos, color, norms, vip1j_x, vip1j_y, vip1j_z, r, g, b, norm);
                add_vertex(coords, pos, color, norms, vijp1_x, vijp1_y, vijp1_z, r, g, b, norm);

                // add triangle
                add_vertex(coords, pos, color, norms, vijp1_x, vijp1_y, vijp1_z, r, g, b, norm2);
                add_vertex(coords, pos, color, norms, vip1jp1_x, vip1jp1_y, vip1jp1_z, r, g, b, norm2);
                add_vertex(coords, pos, color, norms, vip1j_x, vip1j_y, vip1j_z, r, g, b, norm2);
                */
            }
        }
        find_smooth_normals(pos, norms);
        redo_coords(coords, pos, colors, norms);
    }

};

#endif
