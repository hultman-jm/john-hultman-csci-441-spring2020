/*
Author: John Hultman
Class: CSCI 441
Date: 1/28/2020
Description: lab 1 Software Rasterization
/**/

#include <iostream>
#include "bitmap_image.hpp"

using namespace std;
//structure for a point with an x,y coord and a color in the form r,g,b
class Point{
public:
	float x, y, r, g, b;
	//constructor
	Point (float inX, float inY, float inR, float inG, float inB){
		x = inX;
		y = inY;
		r = inR;
		g = inG;
		b = inB;
		//std::cout << "in Point Constructor" << std::endl;
	}
	//default constructor
	Point(){
		x=0;
		y=0;
		r=0;
		g=0;
		b=0;
		//std::cout << "in Null Point Constructor" << std::endl;
	}
};

// after struggling with the barycentric algorithm i found a post on 
//https://gamedev.stackexchange.com/questions/33352/convert-point-to-its-barymetric-coordinates?noredirect=1&lq=1
//the last post in the thread helped me understand the math after seeing it in code
void findBaryCoord(float inX, float inY, Point a, Point b, Point c, float arr[]){
	float v0x = b.x - a.x;
        float v0y = b.y - a.y;
        float v1x = c.x - a.x;
        float v1y = c.y - a.y;
        float v2x = inX - a.x;
        float v2y = inY - a.y;

        float d00 = v0x * v0x + v0y * v0y;
        float d01 = v0x * v1x + v0y * v1y;
        float d11 = v1x * v1x + v1y * v1y;
        float d20 = v2x * v0x + v2y * v0y;
        float d21 = v2x * v1x + v2y * v1y;

        float denom = d00 * d11 - d01 * d01;

        float v = (d11 * d20 - d01 * d21) / denom;
        float w = (d00 * d21 - d01 * d20) / denom;
        float u = 1.0 - v - w;

	arr[0] = v;
	arr[1] = w;
	arr[2] = u;
	//return u >= 0 && v >= 0 && u+v < 1;
}

//method to scale color using barycentric coords
rgb_t scaleColor(rgb_t color1, rgb_t color2, rgb_t color3, float weights[]){
	float r = (weights[0]*color1.red) + (weights[0]*color1.blue) + (weights[0]*color1.green);
	float g = (weights[1]*color2.red) + (weights[1]*color2.blue) + (weights[1]*color2.green);
	float b = (weights[2]*color3.red) + (weights[2]*color3.blue) + (weights[2]*color3.green);

	rgb_t outColor = make_colour(r, g, b);
	return outColor;
}

//operator overload for std::cout << for the point class
std::ostream& operator<<(std::ostream& stream, const Point inP)
{
	stream << "(" << inP.x << ", " << inP.y << ": " << inP.r << ", " << inP.g << ", " << inP.b << ")" << std::endl;
	return stream;
}
int main(int argc, char** argv) {
    /*
      Prompt user for 3 points separated by whitespace.

      Part 1:
          You'll need to get the x and y coordinate as floating point values
          from the user for 3 points that make up a triangle.

      Part 3:
          You'll need to also request 3 colors from the user each having
          3 floating point values (red, green and blue) that range from 0 to 1.
    */
	float x, y, r, g, b;
	Point points [3];
	char eater;
	std::cout << "Please enter 3 points in the form x,y:r,b,g:" << std::endl;
	for (int i = 0; i < 3; i++)
	{
		std::cin >> x >> eater >> y >> eater >> r >> eater >> g >> eater >> b;
		//std::cin.ignore(9, ",");
		points[i] = Point (x, y, r, g, b);
		//std::cout << points[i];
	}

	std::cout << "you entered:" << std::endl;
	std::cout << points[0];
	std::cout << points[1];
	std::cout << points[2];

    // create an image 640 pixels wide by 480 pixels tall
    	bitmap_image image(640, 480);

	rgb_t aColor = make_colour((points[0].r * 255), (points[0].g * 255), (points[0].b * 255));
	rgb_t bColor = make_colour((points[1].r * 255), (points[1].g * 255), (points[1].b * 255));
	rgb_t cColor = make_colour((points[2].r * 255), (points[2].g * 255), (points[2].b * 255));

	for (y = 0; y < 480 ; ++y)
	{
		for (x = 0; x < 640; ++x)
		{
			float tempWeights[3];
			findBaryCoord(x, y, points[0], points[1], points[2], tempWeights);
			//tempWeight={v, w, u} u >= 0 && v >= 0 && u+v < 1
			if (tempWeights[2] >= 0 && tempWeights[0] >= 0 && tempWeights[2]+tempWeights[0] < 1 == true)
			{
				//rgb_t color = make_colour(255, 255, 255);//colors pixel white for testing in part 1 and 2
				rgb_t color = scaleColor(aColor, bColor, cColor, tempWeights);
				image.set_pixel(x,y,color);
			}
		}
	}
    /*
      Part 1:
          Calculate the bounding box of the 3 provided points and loop
          over each pixel in that box and set it to white using:

          rgb_t color = make_color(255, 255, 255);
          image.set_pixel(x,y,color);

      Part 2:
          Modify your loop from part 1. Using barycentric coordinates,
          determine if the pixel lies within the triangle defined by
          the 3 provided points. If it is color it white, otherwise
          move on to the next pixel.

      Part 3:
          For each pixel in the triangle, calculate the color based on
          the calculated barycentric coordinates and the 3 provided
          colors. Your colors should have been entered as floating point
          numbers from 0 to 1. The red, green and blue components range
          from 0 to 255. Be sure to make the conversion.
    */

    image.save_image("triangle.bmp");
    std::cout << "Success" << std::endl;
}
