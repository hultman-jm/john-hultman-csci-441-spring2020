/*
Author: John Hultman
Class: CSCI 441
Date: 2/7/2020
Description: lab 3 Transformation Matrices
*/

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>

int k = 0;

class Matrix
{
public:
	float matrix[16] = {
	1.0,  0.0,  0.0,  0.0,
	0.0,  1.0,  0.0,  0.0,
	0.0,  0.0,  1.0,  0.0,
	0.0,  0.0,  0.0,  1.0
	};
	Matrix()//identity matrix for no transform;
	{
		matrix[0] = 1.0;
		matrix[1] = 0.0;
		matrix[2] = 0.0;
		matrix[3] = 0.0;
		matrix[4] = 0.0;
		matrix[5] = 1.0;
		matrix[6] = 0.0;
		matrix[7] = 0.0;
		matrix[8] = 0.0;
		matrix[9] = 0.0;
		matrix[10] = 1.0;
		matrix[11] = 0.0;
		matrix[12] = 0.0;
		matrix[13] = 0.0;
		matrix[14] = 0.0;
		matrix[15] = 1.0;
	}
	Matrix static scale(Matrix inMatrix, float scaleX, float scaleY)//scale transform matrix
	{
		inMatrix.matrix[0]= scaleX;
		inMatrix.matrix[1] = 0.0;
		inMatrix.matrix[2] = 0.0;
		inMatrix.matrix[3] = 0.0;
		inMatrix.matrix[4] = 0.0;
		inMatrix.matrix[5] = scaleY; 
		inMatrix.matrix[6] = 0.0;
		inMatrix.matrix[7] = 0.0;
		inMatrix.matrix[8] = 0.0;
		inMatrix.matrix[9] = 0.0;
		inMatrix.matrix[10] = 1.0;
		inMatrix.matrix[11] = 0.0;
		inMatrix.matrix[12] = 0.0;
		inMatrix.matrix[13] = 0.0;
		inMatrix.matrix[14] = 0.0;
		inMatrix.matrix[15] = 1.0;

		return inMatrix;
	}
	Matrix static translate(Matrix inMatrix, float transX, float transY)//translate transform matrix
	{
		inMatrix.matrix[0] = 1.0;
		inMatrix.matrix[1] = 0.0;
		inMatrix.matrix[2] = 0.0;
		inMatrix.matrix[3] = transX; 
		inMatrix.matrix[4] = 0.0;
		inMatrix.matrix[5] = 1.0;
		inMatrix.matrix[6] = 0.0;
		inMatrix.matrix[7] = transY; 
		inMatrix.matrix[8] = 0.0;
		inMatrix.matrix[9] = 0.0;
		inMatrix.matrix[10] = 1.0;
		inMatrix.matrix[11] = 0.0;
		inMatrix.matrix[12] = 0.0;
		inMatrix.matrix[13] = 0.0;
		inMatrix.matrix[14] = 0.0;
		inMatrix.matrix[15] = 1.0;

		return inMatrix;
	}
	Matrix static rotate(Matrix inMatrix, float theta)
	{
		inMatrix.matrix[0] = cos(theta);
		inMatrix.matrix[1] = -(sin(theta));
		inMatrix.matrix[2] = 0.0;
		inMatrix.matrix[3] = 0.0;
		inMatrix.matrix[4] = sin(theta);
		inMatrix.matrix[5] = cos(theta);
		inMatrix.matrix[6] = 0.0;
		inMatrix.matrix[7] = 0.0;
		inMatrix.matrix[8] = 0.0;
		inMatrix.matrix[9] = 0.0;
		inMatrix.matrix[10] = 1.0;
		inMatrix.matrix[11] = 0.0;
		inMatrix.matrix[12] = 0.0;
		inMatrix.matrix[13] = 0.0;
		inMatrix.matrix[14] = 0.0;
		inMatrix.matrix[15] = 1.0;

		return inMatrix;
	}
	Matrix static rotateOff(Matrix inMatrix, float theta, float transX, float transY)
	 {
		 inMatrix.matrix[0] = cos(theta);
		 inMatrix.matrix[1] = -(sin(theta));
		 inMatrix.matrix[2] = 0.0;
		 inMatrix.matrix[3] = transX;
		 inMatrix.matrix[4] = sin(theta);
		 inMatrix.matrix[5] = cos(theta);
		 inMatrix.matrix[6] = 0.0;
		 inMatrix.matrix[7] = transY;
		 inMatrix.matrix[8] = 0.0;
		 inMatrix.matrix[9] = 0.0;
		 inMatrix.matrix[10] = 1.0;
		 inMatrix.matrix[11] = 0.0;
		 inMatrix.matrix[12] = 0.0;
		 inMatrix.matrix[13] = 0.0;
		 inMatrix.matrix[14] = 0.0;
		 inMatrix.matrix[15] = 1.0;

		 return inMatrix;
	 }
	Matrix static rotateScale(Matrix inMatrix, float theta, float scaleX, float scaleY)
	 {
		 inMatrix.matrix[0] = cos(theta) * scaleX;
		 inMatrix.matrix[1] = -(sin(theta)) * scaleX;
		 inMatrix.matrix[2] = 0.0;
		 inMatrix.matrix[3] = 0.0;
		 inMatrix.matrix[4] = sin(theta) *scaleY;
		 inMatrix.matrix[5] = cos(theta) * scaleY;
		 inMatrix.matrix[6] = 0.0;
		 inMatrix.matrix[7] = 0.0;
		 inMatrix.matrix[8] = 0.0;
		 inMatrix.matrix[9] = 0.0;
		 inMatrix.matrix[10] = 1.0;
		 inMatrix.matrix[11] = 0.0;
		 inMatrix.matrix[12] = 0.0;
		 inMatrix.matrix[13] = 0.0;
		 inMatrix.matrix[14] = 0.0;
		 inMatrix.matrix[15] = 1.0;

		 return inMatrix;
	 }
};
int menu()
{
	int state;
	std::cout << "please input an integer between 1 and 5: " << std::endl;
	std::cout << "1: static image" << std::endl;
	std::cout << "2: scale" << std::endl;
	std::cout << "3: rotate center" << std::endl;
	std::cout << "4: rotate off center" << std::endl;
	std::cout << "5: rotate on center and scale" << std::endl;
	std::cin >> state;
	switch (state) {
	case 1: //option 1 is to make the image static
		std::cout << "image is now static" << std::endl;
		return k = 0;
		break;
	case 2: //option 2 is scale
		return k = 2;
		break;
	case 3: //option 3 is to rotate on the center
		return k = 1;
		break;
	case 4: //option 4 is to rotate off center
		return k = 3;
		break;
	case 5: //option 4 is to rotate off center
		return k = 4;
		break;
	default:
		std::cout << "not a valid option." << std::endl;
		break;
	}
	//return outMatrix;
}
void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		menu();
	}
}

int main(void) {
    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the triangle drawing */
    // define the vertex coordinates of the triangle
    float triangle[] = {
         0.5f,  0.5f, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,
    };


    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("vert.glsl", "frag.glsl");
	//initialize the uniform "transform"
	Matrix transformMatrix = Matrix();
	int transLoc = glGetUniformLocation(shader.id(), "transform");
	glUniformMatrix4fv(transLoc, 1, GL_FALSE, transformMatrix.matrix);
    /* Loop until the user closes the window */

    while (!glfwWindowShouldClose(window)) {
		//vector2 inVect(glGetFloatv(0, VAO));
        // process input
		float time = glfwGetTime();
        processInput(window);
		

		if (k == 0)
		{
			transformMatrix = Matrix();
			transLoc = glGetUniformLocation(shader.id(), "transform");
			glUniformMatrix4fv(transLoc, 1, GL_FALSE, transformMatrix.matrix);
		}
		else if (k == 1 )
		{
			transformMatrix = Matrix::rotate(transformMatrix, time);
			transLoc = glGetUniformLocation(shader.id(), "transform");
			glUniformMatrix4fv(transLoc, 1, GL_FALSE, transformMatrix.matrix);
		}
		else if (k == 2)
		{
			transformMatrix = Matrix::scale(transformMatrix, sin(time), sin(time));
			transLoc = glGetUniformLocation(shader.id(), "transform");
			glUniformMatrix4fv(transLoc, 1, GL_FALSE, transformMatrix.matrix);
		}
		else if (k == 3)
		{
			transformMatrix = Matrix::rotateOff(transformMatrix, time, sin(time), sin(time));
			transLoc = glGetUniformLocation(shader.id(), "transform");
			glUniformMatrix4fv(transLoc, 1, GL_FALSE, transformMatrix.matrix);
		}
		else if (k == 4)
		{
			transformMatrix = Matrix::rotateScale(transformMatrix, time, sin(time), sin(time));
			transLoc = glGetUniformLocation(shader.id(), "transform");
			glUniformMatrix4fv(transLoc, 1, GL_FALSE, transformMatrix.matrix);
		}
		else 
		{
			k = 0;
		}
        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // use the shader
        shader.use();

        /** Part 2 animate and scene by updating the transformation matrix */

        // draw our triangles
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
