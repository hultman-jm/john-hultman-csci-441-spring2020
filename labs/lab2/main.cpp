/*
Author: John Hultman
Class: CSCI 441
Date: 2/2/2020
Description: lab 2 Hardware Rasterization
/**/

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

/**
 * BELOW IS A BUNCH OF HELPER CODE
 * You do not need to understand what is going on with it, but if you want to
 * know, let me know and I can walk you through it.
 */

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

GLFWwindow* initWindow() {
    GLFWwindow* window;
    if (!glfwInit()) {
        return NULL;
    }

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    window = glfwCreateWindow(640, 480, "Lab 2", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return NULL;
    }

    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return NULL;
    }

    return window;
}

std::string shaderTypeName(GLenum shaderType) {
    switch(shaderType) {
        case GL_VERTEX_SHADER: return "VERTEX";
        case GL_FRAGMENT_SHADER: return "FRAGMENT";
        default: return "UNKNOWN";
    }
}

std::string readFile(const std::string& fileName) {
    std::ifstream stream(fileName);
    std::stringstream buffer;
    buffer << stream.rdbuf();

    std::string source = buffer.str();
    std::cout << "Source:" << std::endl;
    std::cout << source << std::endl;

    return source;
}

/** END OF CODE THAT YOU DON'T NEED TO WORRY ABOUT */

GLuint createShader(const std::string& fileName, GLenum shaderType) {
    std::string source = readFile(fileName);
    const char* src_ptr = source.c_str();

    /** YOU WILL ADD CODE STARTING HERE */
    GLuint shader = 0;
    // create the shader using
    // glCreateShader, glShaderSource, and glCompileShader
	shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &src_ptr, NULL);
	glCompileShader(shader);
    /** END CODE HERE */

    // Perform some simple error handling on the shader
    int success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::" << shaderTypeName(shaderType)
            <<"::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return shader;
}

GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {
    /** YOU WILL ADD CODE STARTING HERE */
    // create the program using glCreateProgram, glAttachShader, glLinkProgram
    GLuint program = 0;
	program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);
    /** END CODE HERE */

    // Perform some simple error handling
    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        char infoLog[512];
        glGetProgramInfoLog(program, 512, NULL, infoLog);
        std::cerr << "ERROR::PROGRAM::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return program;
}

void normalizeCoords(float inTriangle[], float inNormals[])
{
	//point 1 normalized
	inNormals[0] = (-1.0 + (inTriangle[0] * (2.0 / 640.0)));
	inNormals[1] = (1.0 - (inTriangle[1] * (2.0 / 480.0)));
	inNormals[6] = inTriangle[2];
	inNormals[7] = inTriangle[3];
	inNormals[8] = inTriangle[4];

	//point 2 normalized
	inNormals[2] = (-1.0 + (inTriangle[5] * (2.0 / 640.0)));
	inNormals[3] = (1.0 - (inTriangle[6] * (2.0 / 480.0)));
	inNormals[9] = inTriangle[7];
	inNormals[10] = inTriangle[8];
	inNormals[11] = inTriangle[9];

	//point 3 normalized
	inNormals[4] = (-1.0 + (inTriangle[10] * (2.0 / 640.0)));
	inNormals[5] = (1.0 - (inTriangle[11] * (2.0 / 480.0)));
	inNormals[12] = inTriangle[12];
	inNormals[13] = inTriangle[13];
	inNormals[14] = inTriangle[14];
}

int main(void) {
    GLFWwindow* window = initWindow();
    if (!window) {
        std::cout << "There was an error setting up the window" << std::endl;
        return 1;
    }

    /** YOU WILL ADD DATA INITIALIZATION CODE STARTING HERE */

    /* PART1: ask the user for coordinates and colors, and convert to normalized
     * device coordinates */

    // convert the triangle to an array of floats containing
    // normalized device coordinates and color components.
	float triangle[15];
	float normals[15];
	float x, y, r, g, b;

//coords to paste for triangle
/*
50,50:1,0,0
600,50:0,1,0
600,400:0,0,1
*/

	char eater;
	std::cout << "Please enter 3 points in the form x,y:r,b,g (with a window size of 640, 480): " << std::endl;
	std::cin >> x >> eater >> y >> eater >> r >> eater >> g >> eater >> b;
	float point1[] = { x, y, r, g, b };
	std::cin >> x >> eater >> y >> eater >> r >> eater >> g >> eater >> b;
	float point2[] = { x, y, r, g, b };
	std::cin >> x >> eater >> y >> eater >> r >> eater >> g >> eater >> b;
	float point3[] = { x, y, r, g, b };

	for (int i = 0; i < 3; i++)
	{
		if (i == 0)
		{
			for (int j = 0; j < 5; j++)
			{
				triangle[j] = point1[j];
			}
		}
		if (i == 1)
		{
			for (int j = 0; j < 5; j++)
			{
				triangle[5+j] = point2[j];
			}
		}
		if (i == 2)
		{
			for (int j = 0; j < 5; j++)
			{
				triangle[10+j] = point3[j];
			}
		}
	}
	std::cout << "you entered:" << std::endl;
	for (int i = 0; i < 15; i++)
	{
		std::cout << triangle[i] << ", ";
	}

	normalizeCoords(triangle, normals);
	std::cout << std::endl;
	std::cout << "your converted normalized coords:" << std::endl;
	for (int i = 0; i < 15; i++)
	{
		std::cout << normals[i] << ", ";
	}

    /** PART2: map the data */

    // create vertex and array buffer objects using
    // glGenBuffers, glGenVertexArrays
    GLuint VBO[1], VAO[1];
	glGenVertexArrays(1, &VAO[0]);
	glGenBuffers(1, &VBO[0]);

    // setup triangle using glBindVertexArray, glBindBuffer, GlBufferData
	glBindVertexArray(VAO[0]);

	glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(normals), normals, GL_STATIC_DRAW);

	glBufferData(GL_ARRAY_BUFFER, sizeof(normals), normals, GL_STATIC_DRAW);
    // setup the attribute pointer for the coordinates
    // setup the attribute pointer for the colors
    // both will use glVertexAttribPointer and glEnableVertexAttribArray;
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	

    /** PART3: create the shader program */

    // create the shaders
    // YOU WILL HAVE TO ADD CODE TO THE createShader FUNCTION ABOVE
    GLuint vertexShader = createShader("vert.glsl", GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader("frag.glsl", GL_FRAGMENT_SHADER);

	//-- uncomment if the filestructure needs to move out up a directory to access the shader files --
	/* 
	GLuint vertexShader = createShader("..vert.glsl", GL_VERTEX_SHADER);
	GLuint fragmentShader = createShader("..frag.glsl", GL_FRAGMENT_SHADER);
	*/

    // create the shader program
    // YOU WILL HAVE TO ADD CODE TO THE createShaderProgram FUNCTION ABOVE
    GLuint shaderProgram = createShaderProgram(vertexShader, fragmentShader);

    // cleanup the vertex and fragment shaders using glDeleteShader

    /** END INITIALIZATION CODE */

    while (!glfwWindowShouldClose(window)) {
        // you don't need to worry about processInput, all it does is listen
        // for the escape character and terminate when escape is pressed.
        processInput(window);

        /** YOU WILL ADD RENDERING CODE STARTING HERE */
        /** PART4: Implemting the rendering loop */

        // clear the screen with your favorite color using glClearColor
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // set the shader program using glUseProgram
		glUseProgram(shaderProgram);
        // bind the vertex array using glBindVertexArray
		glBindVertexArray(VAO[0]);
        // draw the triangles using glDrawArrays
		glDrawArrays(GL_TRIANGLES, 0, 3);

        /** END RENDERING CODE */

        // Swap front and back buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
