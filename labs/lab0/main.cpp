/*
Author: John Hultman
Class: CSCI 441
Date: 1/19/2020
Description: lab 0 - Up and running with C++
/**/

#include <iostream>
#include <string>

using namespace std;

class Vector3 {
public:
    	float x;
    	float y;
    	float z;

    	// Constructor
    	Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {
        	// nothing to do here as we've already initialized x, y, and z above
        	std::cout << "in Vector3 constructor" << std::endl;
    	}

	Vector3() {
        	// sets  the x,y, z to 0 as a null. 
        	float x = 0;
		float y = 0;
		float z = 0;
		std::cout << "in Vector3 default constructor" << std::endl;
    	}

    	// Destructor - called when an object goes out of scope or is destroyed
    	~Vector3() {
        	// this is where you would release resources such as memory or file descriptors
        	// in this case we don't need to do anything
        	std::cout << "in Vector3 destructor" << std::endl;
	}

};

//pass by value vecotr3 add() fuction
Vector3 add(Vector3 v, Vector3 v2) { // v and v2 are copies, so any changes to them in this function
                                     // won't affect the originals
	float outX = v.x + v2.x;
	float outY = v.y + v2.y;
	float outZ = v.z+ v2.z;
	Vector3 outV(outX, outY, outZ);

	return outV;
}

//pass by value vector3 opperator overload of opeerator+
Vector3 operator + (Vector3 v, Vector3 v2) {
	float outX = v.x + v2.x;
	float outY = v.y + v2.y;
	float outZ = v.z + v2.z;
	Vector3 outV(outX, outY, outZ);

	return outV;
}
/**/

/*
//pass by reference vector3 add() function
Vector3 add(const Vector3 v, const Vector3 v2) { // v and v2 are copies, so any changes to them in this function
                                     // won't affect the originals
	float outX = v.x + v2.x;
	float outY = v.y + v2.y;
	float outZ = v.z+ v2.z;
	Vector3 outV(outX, outY, outZ);

	return outV;
}

//pass by reference vector3 opperator overload of opeerator+
Vector3 operator + (const Vector3 v, const Vector3 v2) {
	float outX = v.x + v2.x;
	float outY = v.y + v2.y;
	float outZ = v.z + v2.z;
	Vector3 outV(outX, outY, outZ);

	return outV;
}
/**/

//operator overload of operator<<
std::ostream& operator<<(std::ostream& stream, const Vector3& v) {
    	// std::cout is a std::ostream object, just like stream
    	// so use stream as if it were cout and output the components of
    	// the vector
	stream << v.x << ", " << v.y << ", " << v.z << std::endl;
    	return stream;
}

int main(int argc, char** argv) {
	string inName;//variable to save the inputted name

	std::cout << "Please enter your name> ";
	std::cin >> inName;//reads in user inputted name
	std::cout << "hello "<< inName << std::endl;
	Vector3 a(1,2,3);   // allocated to the stack
	Vector3 b(4,5,6);

	Vector3 c = add(a,b); // copies 6 floats to the arguments in add (3 per Vector3),
                          // 3 more floats copied into c when it returns
                          // a and b are unchanged
	//prints vector c the total of vectors a and b
	std::cout << "Vector3 c = (" << c.x << ", " << c.y << ", " << c.z << ")" << std::endl;

	Vector3 v(1,2,3);
	Vector3 v2(4,5,6);

	std::cout << v+v2 << std::endl;// prints the total of vectors v and v2 using the overloaded operator+ and operator<<

	Vector3 v3(0,0,0);
	v3.y = 5;//v3's y value is set to 5 without pointers
	std::cout << v3 << std::endl;

	Vector3* v4 = new Vector3(0,0,0);
	v4-> y=5;//v4's y value is set to 5 using pointers
	std::cout << *v4 << std::endl;
	delete v4;

	Vector3 array[10];//initilaize a vector3 array of size 10 with a value of (0,0,0)
	Vector3* arrayOfVectors = new Vector3[10];// loop to set each y value of each value of the array to 5, making every value of the array equal to (0,5,0)
	for( int a = 1; a < 10; a = a + 1 ) {//loop also prints each value of the array using the overloaded operrator<<
      		arrayOfVectors[a].y = 5;
		std:cout << arrayOfVectors[a] << std::endl;
   	}
	delete [] arrayOfVectors;

	Vector3& v5 = v;//creates a reference, v5, of vector3 v
	Vector3& v6 = v2;//creates a reference, v6, of vector3 v2

	Vector3 v7 = add(v5,v6);//when the pass by reference function is uncommented this will set v7 to the return value
	std::cout << "Vector3 v7 = (" << v7.x << ", " << v7.y << ", " << v7.z << ")" << std::endl;

	std::cout << v5 + v6 << std::endl;

}
